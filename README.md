# Installation #

## System Requirements ##
Android (4.0 or higher)

You must enable installing applications from unknown sources in your security settings.

**This application will only work when connected to the UNCC network, either via VPN or Wi-Fi.**

## Installation Process ##

Download the apk file located at [url] from your phone. That's it!

## Configuring a UNCC VPN 
Download a VPN Client such as [Cisco AnyConnect](https://play.google.com/store/apps/details?id=com.cisco.anyconnect.vpn.android.avf&hl=en) or [OpenConnect](https://play.google.com/store/apps/details?id=app.openconnect&hl=en).

### Cisco AnyConnect ###
After downloading Cisco AnyConnect, you will need to add a new VPN profile. Use vpn.uncc.edu/students for the server address. Then, you can simply press the AnyConnect VPN switch to enable the VPN, and you will be presented with a login prompt. Enter your NinerNET credentials and you should now have a working VPN running.

![CiscoAnyConnectProfile_small.png](https://bitbucket.org/repo/KBpxap/images/3020874421-CiscoAnyConnectProfile_small.png)
![CiscoAnyConnectLoginScreenshot_small.png](https://bitbucket.org/repo/KBpxap/images/2160511937-CiscoAnyConnectLoginScreenshot_small.png)

### OpenConnect ###
After downloading OpenConnect, you will need to add a new VPN profile. Use vpn.uncc.edu/students for the server address. Unlike Cisco AnyConnect, OpenConnect presents you with a notice stating that you acknowledge that all of your traffic going through the VPN may be monitored by a third party. Press OK and you will be presented with a login prompt. Enter your NinerNET credentials and you should now have a working VPN running.

![OpenConnectProfileScreenshot_small.png](https://bitbucket.org/repo/KBpxap/images/1240511325-OpenConnectProfileScreenshot_small.png)
![OpenConnectConnectionRequest_small.png](https://bitbucket.org/repo/KBpxap/images/824358364-OpenConnectConnectionRequest_small.png)
![OpenConnectLoginScreenshot_small.png](https://bitbucket.org/repo/KBpxap/images/3692634522-OpenConnectLoginScreenshot_small.png)

## Using the Application ##
Upon opening the application the first thing you will be greeted by is the items screen. On the bottom half of this screen is a scrollable list of all of the items owned by STARS and kept in the cabinet. When you click on an item, you will be able to view how many items there are of this type total, and how many are left that can be checked out. You can also view the user logs, and search through these logs by name. These logs tell you who did something, when they did it, and what exactly they did. There is also an about page which will send you to the STARS twitter, facebook, etc.

![ItemsScreenScreenshot_small.png](https://bitbucket.org/repo/KBpxap/images/90469761-ItemsScreenScreenshot_small.png)
![LogsScreen_small.png](https://bitbucket.org/repo/KBpxap/images/3142251075-LogsScreen_small.png)
![AboutPageScreenshot_small.png](https://bitbucket.org/repo/KBpxap/images/1667153784-AboutPageScreenshot_small.png)