package com.example.aes.starsassetmanagement;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ItemFragment extends Fragment {
    /** Fragment Initialization Parameters **/
    private static final String ARG_AMOUNT = "amount";
    private static final String ARG_REMAINING = "remaining";

    private String itemAmount;
    private String amountRemaining;

    /** View Variables **/
    @BindView(R.id.textview_items_remaining) TextView mItemRemaining;
    @BindView(R.id.textview_item_amount) TextView mItemAmount;
    private Unbinder unbinder;

    public ItemFragment() {
        // Required empty public constructor
    }

    public static ItemFragment newInstance(String amount, String remaining) {
        ItemFragment fragment = new ItemFragment();
        Bundle args = new Bundle();
        args.putString(ARG_AMOUNT, amount);
        args.putString(ARG_REMAINING, remaining);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemAmount = getArguments().getString(ARG_AMOUNT);
            amountRemaining = getArguments().getString(ARG_REMAINING);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item, container, false);
        unbinder = ButterKnife.bind(this, view);
        String s;
        s = "<b>Amount Remaining:</b> " + amountRemaining;
        mItemRemaining.setText(Html.fromHtml(s));
        s = "<b>Total Available:</b> " + itemAmount;
        mItemAmount.setText(Html.fromHtml(s));
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
