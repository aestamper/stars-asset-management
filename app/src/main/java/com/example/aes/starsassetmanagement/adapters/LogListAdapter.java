package com.example.aes.starsassetmanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.aes.starsassetmanagement.R;
import com.example.aes.starsassetmanagement.models.LogEntry;

import java.util.ArrayList;

public class LogListAdapter extends ArrayAdapter<LogEntry> {
    private ArrayList<LogEntry> mLogList;
    private Context mContext;
    private int mResource;

    public LogListAdapter(Context context, int resource, ArrayList<LogEntry> logs) {
        super(context, resource, logs);
        this.mContext = context;
        this.mLogList = logs;
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }
        LogEntry logEntry = mLogList.get(position);
        TextView timeStamp = (TextView) convertView.findViewById(R.id.textview_log_timestamp);
        timeStamp.setText(logEntry.getPrettyTimeStamp());
        TextView name = (TextView) convertView.findViewById(R.id.textview_log_name);
        name.setText(logEntry.getFullName());
        TextView details = (TextView) convertView.findViewById(R.id.textview_log_details);
        details.setText(logEntry.getDetails());
        return convertView;
    }
}
