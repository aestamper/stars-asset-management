package com.example.aes.starsassetmanagement.database;

public abstract class ItemsEntry {
    public static final String TABLE_NAME = "Items";
    public static final String COLUMN_NAME_ITEM_ID = "item_id";
    public static final String COLUMN_NAME_ITEM_NAME = "item_name";
    public static final String COLUMN_NAME_ITEM_AMOUNT = "item_amount";
    public static final String COLUMN_NAME_ITEM_REMAINING = "item_remaining";
}
