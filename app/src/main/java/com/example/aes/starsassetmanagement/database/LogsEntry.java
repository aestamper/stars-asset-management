package com.example.aes.starsassetmanagement.database;

import android.provider.BaseColumns;

public abstract class LogsEntry implements BaseColumns {
    public static final String TABLE_NAME = "Logs";
    public static final String COLUMN_NAME_LOG_ID = "log_id";
    public static final String COLUMN_NAME_LOG_TIME_STAMP = "log_time_stamp";
    public static final String COLUMN_NAME_LOG_USER_NAME = "log_user_name";
    public static final String COLUMN_NAME_LOG_INFORMATION = "log_information";
}
