package com.example.aes.starsassetmanagement;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Element versionElement = new Element();
        versionElement.setTitle("Version 1.0.0");

        Element adsElement = new Element();
        adsElement.setTitle("Support STARS today!");

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setImage(R.drawable.ic_stars_logo)
                .setDescription("The STARS Leadership Corps (Corps) is a multi-year experience providing students with support throughout their academic journey.")
                .addItem(versionElement)
                .addItem(adsElement)
                .addGroup("Connect with us")
                .addFacebook("unccstars")
                .addTwitter("STARSCorps")
                .addYoutube("STARSComputing")
                .create();

        setContentView(aboutPage);
    }
}
