package com.example.aes.starsassetmanagement.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogEntry {

    @SerializedName("ID")
    @Expose
    private Long ID;

    @SerializedName("Timestamp")
    @Expose
    private String timeStamp;

    @SerializedName("PrettyTimestamp")
    @Expose
    private String prettyTimeStamp;

    @SerializedName("User")
    @Expose
    private String fullName;

    @SerializedName("Information")
    @Expose
    private String details;

    public LogEntry(long id, String time, String name, String d) {
        ID = id;
        prettyTimeStamp = time;
        fullName = name;
        details = d;
    }

    public String getPrettyTimeStamp() {
        return prettyTimeStamp;
    }

    public void setPrettyTimeStamp(String prettyTimeStamp) {
        this.prettyTimeStamp = prettyTimeStamp;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    //public String getTimeStamp() {
    //    return timeStamp;
    //}

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
