package com.example.aes.starsassetmanagement.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aes.starsassetmanagement.R;
import com.example.aes.starsassetmanagement.models.Item;

import java.util.ArrayList;

public class ItemListAdapter extends ArrayAdapter<Item> {
    private ArrayList<Item> mItems;
    private Context mContext;
    private int mResource;

    public ItemListAdapter(Context context, int resource, ArrayList<Item> items) {
        super(context, resource, items);
        this.mContext = context;
        this.mItems = items;
        this.mResource = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mResource, parent, false);
        }
        Item item = mItems.get(position);
        if (item.getName().equals("Laptop")) {
            ImageView laptopImage = (ImageView) convertView.findViewById(R.id.availabilityImage);
            Drawable drawable = mContext.getResources().getDrawable(R.drawable.ic_computer_black_24dp);
            laptopImage.setImageDrawable(drawable);
        } else if (item.getName().equals("IPhone")) {
            ImageView laptopImage = (ImageView) convertView.findViewById(R.id.availabilityImage);
            Drawable drawable = mContext.getResources().getDrawable(R.drawable.ic_phone_android_black_24dp);
            laptopImage.setImageDrawable(drawable);
        }
        TextView itemName = (TextView) convertView.findViewById(R.id.itemName);
        itemName.setText(item.getName());
        return convertView;
    }
}
