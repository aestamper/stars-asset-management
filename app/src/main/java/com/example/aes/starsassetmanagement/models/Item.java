package com.example.aes.starsassetmanagement.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Item implements Serializable {

    @SerializedName("ItemID")
    @Expose
    private long itemId;

    @SerializedName("Name")
    @Expose
    private String name;

    @SerializedName("Barcode")
    @Expose
    private String barcode;

    @SerializedName("ItemAmount")
    @Expose
    private int itemAmount;

    @SerializedName("AmountRemaining")
    @Expose
    private int amountRemaining;

    public Item(long id, String n, String b, int i, int a) {
        itemId = id;
        name = n;
        barcode = b;
        itemAmount = i;
        amountRemaining = a;
    }

    public long getItemId() {
        return itemId;
    }

    public void setItemId(long id) {
        itemId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String n) {
        name = n;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String b) {
        barcode = b;
    }

    public int getItemAmount() {
        return itemAmount;
    }

    public void setItemAmount(int i) {
        itemAmount = i;
    }

    public int getAmountRemaining() {
        return amountRemaining;
    }

    public void setAmountRemaining(int a) {
        amountRemaining = a;
    }
}
