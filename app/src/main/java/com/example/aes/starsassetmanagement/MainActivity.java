package com.example.aes.starsassetmanagement;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aes.starsassetmanagement.API.STARSAPIService;
import com.example.aes.starsassetmanagement.API.ServiceGenerator;
import com.example.aes.starsassetmanagement.adapters.ItemListAdapter;
import com.example.aes.starsassetmanagement.database.DbHelper;
import com.example.aes.starsassetmanagement.models.Item;
import com.github.ybq.android.spinkit.style.Wave;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    /** Database Variables **/
    private DbHelper dbHelper;
    private SQLiteDatabase db;

    /** View Variables **/
    @BindView(R.id.sync_progress) ProgressBar mProgressBar;
    @BindView(R.id.list_items) ListView mListView;
    @BindView(R.id.toolbar) Toolbar toolbar;

    private ArrayList<Item> itemsList;
    private Item item;
    private ItemListAdapter adapter;
    private FragmentManager fragmentManager;
    private boolean submenu;

    ItemFragment fragment = new ItemFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        /** Custom Progress Bar **/
        Wave wave = new Wave();
        mProgressBar.setIndeterminateDrawable(wave);

        setSupportActionBar(toolbar);

        dbHelper = new DbHelper(this);
        db = dbHelper.getWritableDatabase();

        fragmentManager = getSupportFragmentManager();

        if (dbHelper.checkDatabase() && !isOnline()) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            updateItemsViews(dbHelper.queryAllItems());
        } else if (!dbHelper.checkDatabase() && !isOnline()) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        } else if (!dbHelper.checkDatabase() && isOnline()) {
            new DownloadItemsTask().execute();
        } else if (dbHelper.checkDatabase() && isOnline()) {
            updateItemsViews(dbHelper.queryAllItems());
            new DownloadItemsTask().execute();
        }
    }

    /**
     * Checks if the device is connected to a network (not in airplane mode)
     * @return true if the device is connected to a network
     */
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logs) {
            Intent intent = new Intent(this, LogsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private class DownloadItemsTask extends AsyncTask<Void, Integer, Void> { /* <Params, Progress, Result> */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            STARSAPIService service = ServiceGenerator.createService(STARSAPIService.class);
            Call<ArrayList<Item>> call = service.getItems();

            call.enqueue(new Callback<ArrayList<Item>>() {
                @Override
                public void onResponse(Call<ArrayList<Item>> call, Response<ArrayList<Item>> response) {
                    if (response.isSuccessful()) {
                        itemsList = response.body();
                        dbHelper.insertItems(itemsList);
                        updateItemsViews(itemsList);
                    } else {
                        Toast.makeText(getApplicationContext(), "HTTP Status Code " + response.code(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<Item>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Server Unreachable", Toast.LENGTH_LONG).show();
                    Log.d("Error", t.getMessage());
                }
            });
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void updateItemsViews(ArrayList<Item> response) {
        itemsList = response;
        adapter = new ItemListAdapter(MainActivity.this, R.layout.row_item_layout, response);
        mListView.setAdapter(adapter);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                item = itemsList.get(position);
                ItemFragment f = fragment.newInstance(Integer.toString(item.getItemAmount()), Integer.toString(item.getAmountRemaining()));
                if (!submenu) {
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.replace(R.id.fragment_container, f);
                    ft.commit();
                    submenu = true;
                } else if (submenu) {
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    ft.remove(fragment);
                    ft.replace(R.id.fragment_container, f);
                    ft.commit();
                    submenu = false;
                }
            }
        });
        adapter.setNotifyOnChange(true);
    }
}
