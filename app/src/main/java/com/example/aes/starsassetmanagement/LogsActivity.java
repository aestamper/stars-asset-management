package com.example.aes.starsassetmanagement;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aes.starsassetmanagement.API.STARSAPIService;
import com.example.aes.starsassetmanagement.API.ServiceGenerator;
import com.example.aes.starsassetmanagement.adapters.LogListAdapter;
import com.example.aes.starsassetmanagement.database.DbHelper;
import com.example.aes.starsassetmanagement.models.LogEntry;
import com.github.ybq.android.spinkit.style.Wave;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LogsActivity extends AppCompatActivity {

    /** Database Variables **/
    private DbHelper dbHelper;
    private SQLiteDatabase db;
    @BindString(R.string.db_full_path) String DB_FULL_PATH;

    /** View Variables **/
    private ListView mLogsList;
    private ProgressBar mProgressBar;
    private SearchView searchView;
    private MenuItem mSearchMenu;

    private ArrayList<LogEntry> logsList;
    private LogListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);
        ButterKnife.bind(this);

        mProgressBar = (ProgressBar) findViewById(R.id.sync_progress);
        Wave wave = new Wave();
        mProgressBar.setIndeterminateDrawable(wave);

        mLogsList = (ListView) findViewById(R.id.listview_logs);

        dbHelper = new DbHelper(this);
        db = dbHelper.getWritableDatabase();

        if (dbHelper.checkDatabase() && !isOnline()) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
            updateLogsView(dbHelper.queryAllLogs());
        } else if (!dbHelper.checkDatabase() && !isOnline()) {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        } else if (!dbHelper.checkDatabase() && isOnline()) {
            new DownloadLogsTask().execute();
        } else if (dbHelper.checkDatabase() && isOnline()) {
            updateLogsView(dbHelper.queryAllLogs());
            new DownloadLogsTask().execute();
        }
    }

    /**
     * Checks if the device is connected to a network (not in airplane mode)
     * @return true if the device is connected to a network
     */
    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            updateLogsView(dbHelper.queryLogsByName(query));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.options_menu, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(getApplicationContext(), LogsActivity.class)));
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                updateLogsView(dbHelper.queryLogsByName(query));
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        ImageView closeButton = (ImageView) searchView.findViewById(R.id.search_close_btn);
        mSearchMenu = menu.findItem(R.id.search);

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = (EditText) findViewById(R.id.search_src_text);
                et.setText(""); // clear text
                searchView.setQuery("", false); // clear query
                searchView.onActionViewCollapsed(); // collapse the action view
                mSearchMenu.collapseActionView(); // collapse search widget
                updateLogsView(dbHelper.queryAllLogs());
            }
        });

        return true;
    }

    private class DownloadLogsTask extends AsyncTask<Void, Integer, Void> { /* <Params, Progress, Result> */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            STARSAPIService service = ServiceGenerator.createService(STARSAPIService.class);
            Call<ArrayList<LogEntry>> call = service.getLogs();

            call.enqueue(new Callback<ArrayList<LogEntry>>() {
                @Override
                public void onResponse(Call<ArrayList<LogEntry>> call, Response<ArrayList<LogEntry>> response) {
                    if (response.isSuccessful()) {
                        logsList = response.body();
                        dbHelper.insertLogs(logsList);
                        updateLogsView(logsList);
                    } else {
                        Toast.makeText(getApplicationContext(), "HTTP Status Code " + response.code(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<LogEntry>> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "Server Unreachable", Toast.LENGTH_LONG).show();
                    Log.d("Error", t.getMessage());
                }
            });
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
        }

        @Override
        protected void onCancelled(Void logs) {
            super.onCancelled(logs);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void updateLogsView(ArrayList<LogEntry> response) {
        if (adapter == null) {
            adapter = new LogListAdapter(LogsActivity.this, R.layout.row_log_layout, response);
            mLogsList.setAdapter(adapter);
        } else {
            adapter.clear();
            adapter.addAll(response);
            adapter.notifyDataSetChanged();
        }
        adapter.setNotifyOnChange(true);
    }
}
