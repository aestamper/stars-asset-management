package com.example.aes.starsassetmanagement.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.aes.starsassetmanagement.R;
import com.example.aes.starsassetmanagement.models.Item;
import com.example.aes.starsassetmanagement.models.LogEntry;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;

public class DbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "STARS.sqlite";
    private static final int DATABASE_VERSION = 1; // increment on schema changes
    // TODO: CHANGE THIS TO USE "String filePath = (context.getDatabasePath(STARS.SQLite)).getPath();"
    private static final String DB_FULL_PATH = "/data/data/com.example.aes.starsassetmanagement/databases/STARS.sqlite";

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    private static final String CREATE_ITEMS = ""
            + "CREATE TABLE " + ItemsEntry.TABLE_NAME + "("
            + ItemsEntry.COLUMN_NAME_ITEM_ID + " INTEGER PRIMARY KEY NOT NULL,"
            + ItemsEntry.COLUMN_NAME_ITEM_NAME + " TEXT NOT NULL,"
            + ItemsEntry.COLUMN_NAME_ITEM_REMAINING + " INTEGER NOT NULL,"
            + ItemsEntry.COLUMN_NAME_ITEM_AMOUNT + " INTEGER NOT NULL"
            + ")";

    private static final String CREATE_LOGS = ""
            + "CREATE TABLE " + LogsEntry.TABLE_NAME + "("
            + LogsEntry.COLUMN_NAME_LOG_ID + " INTEGER PRIMARY KEY NOT NULL,"
            + LogsEntry.COLUMN_NAME_LOG_TIME_STAMP + " TEXT NOT NULL,"
            + LogsEntry.COLUMN_NAME_LOG_USER_NAME + " TEXT NOT NULL COLLATE NOCASE,"
            + LogsEntry.COLUMN_NAME_LOG_INFORMATION + " TEXT NOT NULL"
            + ")";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ITEMS);
        db.execSQL(CREATE_LOGS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean insertItems(ArrayList<Item> items) {
        SQLiteDatabase db = this.getWritableDatabase();
        for (Item i : items) {
            long itemId = i.getItemId();
            String name = i.getName();
            int itemAmount = i.getItemAmount(), itemRemaining = i.getAmountRemaining();

            ContentValues values = new ContentValues();
            values.put(ItemsEntry.COLUMN_NAME_ITEM_ID, itemId);
            values.put(ItemsEntry.COLUMN_NAME_ITEM_NAME, name);
            values.put(ItemsEntry.COLUMN_NAME_ITEM_REMAINING, itemRemaining);
            values.put(ItemsEntry.COLUMN_NAME_ITEM_AMOUNT, itemAmount);

            db.insert(ItemsEntry.TABLE_NAME, null, values);
        }
        db.close(); // close database connection
        return false;
    }

    public boolean insertLogs(ArrayList<LogEntry> logs) {
        SQLiteDatabase db = this.getWritableDatabase();
        SQLiteDatabase rdb = this.getReadableDatabase();
        Long latestLogID = null;

        String query = "SELECT MAX(" + LogsEntry.COLUMN_NAME_LOG_ID + ") FROM " + LogsEntry.TABLE_NAME + ";";
        Cursor cursor = rdb.rawQuery(query, null);
        if (cursor.getCount() >= 1)
            while (cursor.moveToNext())
                latestLogID = cursor.getLong(0);
        cursor.close();

        for (LogEntry log : logs) {
            long id = log.getID();
            if (latestLogID != null && id <= latestLogID); // nothing to do here
            else {
                ContentValues values = new ContentValues();
                values.put(LogsEntry.COLUMN_NAME_LOG_ID, id);
                values.put(LogsEntry.COLUMN_NAME_LOG_TIME_STAMP, log.getPrettyTimeStamp());
                values.put(LogsEntry.COLUMN_NAME_LOG_USER_NAME, log.getFullName());
                values.put(LogsEntry.COLUMN_NAME_LOG_INFORMATION, log.getDetails());
                long newRowId = db.insert(LogsEntry.TABLE_NAME, null, values);
            }
        }
        rdb.close();
        db.close(); // close database connection
        return false;
    }

    /**
     * Queries the local SQLite database for all of the items.
     *
     * @return A list of all of the item entries
     */
    public ArrayList<Item> queryAllItems() {
        ArrayList<Item> itemsList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + ItemsEntry.TABLE_NAME + " ORDER BY "
                + ItemsEntry.COLUMN_NAME_ITEM_NAME + " DESC";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Item item = new Item(Integer.parseInt(cursor.getString(0)),
                        cursor.getString(1), "",
                        Integer.parseInt(cursor.getString(2)),
                        Integer.parseInt(cursor.getString(3)));
                itemsList.add(item);
            } while(cursor.moveToNext());
        }
        cursor.close();
        db.close(); // close database connection
        return itemsList;
    }

    /**
     * Queries the local SQLite database for all of the logs.
     * @return A list of all of the log entries
     */
    public ArrayList<LogEntry> queryAllLogs() {
        ArrayList<LogEntry> logsList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + LogsEntry.TABLE_NAME + " ORDER BY "
                + LogsEntry.COLUMN_NAME_LOG_ID + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) { // loop through all rows
            do {
                LogEntry log = new LogEntry(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                logsList.add(log); // add log to list
            } while(cursor.moveToNext());
        }
        cursor.close(); db.close();
        return logsList;
    }

    /**
     * Allows the user to search the logs by username and return the corresponding database entries
     *
     * @return The logs pertaining to the username searched for
     */
    public ArrayList<LogEntry> queryLogsByName(String name) {
        ArrayList<LogEntry> logsList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + LogsEntry.TABLE_NAME + " WHERE "
                + LogsEntry.COLUMN_NAME_LOG_USER_NAME + " LIKE '%" + name + "%'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                LogEntry log = new LogEntry(cursor.getLong(0), cursor.getString(1), cursor.getString(2), cursor.getString(3));
                logsList.add(log);
            } while(cursor.moveToNext());
        }
        cursor.close(); db.close();
        return logsList;
    }

    /**
     * Check if the local database exists and can be read
     *
     * @return true if it exists and can be read, otherwise false
     */
    public boolean checkDatabase() {
        SQLiteDatabase checkDB = null;
        try {
            checkDB = SQLiteDatabase.openDatabase(DB_FULL_PATH, null, SQLiteDatabase.OPEN_READONLY);
            checkDB.close();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return checkDB != null;
    }
}
