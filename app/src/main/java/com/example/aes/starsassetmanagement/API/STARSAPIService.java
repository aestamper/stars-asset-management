package com.example.aes.starsassetmanagement.API;

import com.example.aes.starsassetmanagement.models.Item;
import com.example.aes.starsassetmanagement.models.LogEntry;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface STARSAPIService {

    @GET("ItemTable?table=ITEMS")
    Call<ArrayList<Item>> getItems();

    @GET("ItemTable?table=ULOGS")
    Call<ArrayList<LogEntry>> getLogs();
}
